--
-- Daten für Tabelle `tbl_sockel`
--

INSERT INTO `tbl_sockel` (`Sockel_ID`, `Sockel_Typ`, `Pins`) VALUES
(1, '1151 v2', '1151'),
(2, '2066', '2066'),
(3, '1150', '1150'),
(4, '1151', '1151'),
(5, '1155', '1155'),
(6, '1156', '1156'),
(7, '1356', '1356'),
(8, '1366', '1366'),
(9, '1440', '1440'),
(10, '1567', '1567'),
(11, 'AM4', '1331'),
(12, 'AM3+', '940'),
(13, 'LGA 1155', '1155'),
(14, 'LGA 1151', '1511'),
(15, 'LGA 1150', '1150'),
(16, 'LGA 1156', '1156'),
(17, 'LGA 1366', '1366'),
(18, 'LGA 1356', '1356'),
(19, 'LGA 2066', '2066'),
(20, 'LGA 3647', '3647');

--
-- Daten für Tabelle `tbl_cpu`
--

INSERT INTO `tbl_cpu` (`Cpu_Id`, `Sockel_ID`, `Name`, `Multithread`, `Kerne`, `Threats`, `Taktrate`, `Turbotakt`, `Leistung`, `Hersteller`, `Preis`) VALUES
(2, 2, 'I9', 40, '18', '32', '3.00GHz', '4.60GHz', '165W', 'Intel', 1129),
(3, 3, 'I7-4790K', 16, '4', '8', '4.00GHz', '4.40GHz', '88W', 'Intel', 419),
(4, 4, 'I7-7700K', 14, '4', '8', '4.20GHz', '4.50GHz', '91W', 'Intel', 399),
(5, 5, 'I7-3770K', 8, '4', '8', '3.50GHz', '3.90GHz', '77W', 'Intel', 439),
(6, 6, 'I3-540', 4, '2', '4', '3.06GHz', '3.06GHz', '73W', 'Intel', 4.95),
(7, 7, 'Xeon-E5-2430-v2', 24, '6', '12', '2.50GHz', '3.00GHz', '80W', 'Intel', 595.25),
(8, 8, 'Xeon-DP-E5645', 16, '6', '12', '2.40GHz', '2.67GHz', '80W', 'Intel', 95.9),
(9, 9, 'Xeon-E3-1505M-v', 14, '4', '8', '2.80GHz', '3.70GHz', '45W', 'Intel', 169.63),
(10, 10, 'Xeon-MP-E7520', 8, '4', '8', '1.86GHz', '1.86GHz', '95W', 'Intel', 889.08),
(11, 1, 'Ryzen 3 2200G', 1, '4', '4', '3,6 GhZ', '3,9GhZ', '10W', 'AMD', 70),
(12, 1, 'Ryzen 9 3900X', 1, '12', '24', '3.8 Ghz', '4.6 Ghz', '105W', 'AMD', 479),
(13, 1, 'Ryzen 7 2700', 1, '8', '16', '3.2 Ghz', '4.10 Ghz', '65W', 'AMD', 143),
(14, 1, 'Ryzen 5 3600', 1, '6', '12', '3.60 Ghz', '4.20 Ghz', '65W', 'AMD', 175.4),
(15, 2, 'FX-6300', 0, '6', '6', '3.5 Ghz', '4.10 Ghz', '95W', 'AMD', 35),
(16, 2, 'FX-8350', 1, '8', '8', '4 Ghz', '4.2 Ghz', '100W', 'AMD', 89.95),
(17, 2, 'FX-4300', 0, '4', '4', '3.8 Ghz', '4.00 Ghz', '95W', 'AMD', 26.99),
(18, 4, 'i7 9700K', 1, '8', '8', '3.6 Ghz', '4.9 Ghz', '95W', 'Intel', 397.9),
(19, 9, 'i9 9900X', 1, '10', '20', '3.5 Ghz', '4.4 Ghz', '165W', 'Intel', 871.1),
(20, 4, 'i5 9600K', 1, '6', '6', '3.7 Ghz', '4.6 Ghz', '95W', 'Intel', 239);


--
-- Daten für Tabelle `tbl_pci_e_slot`
--

INSERT INTO `tbl_pci_e_slot` (`PCI_e_ID`, `PCI_e_slot`, `Version`, `PCIe_Groesse`) VALUES
(1, 10, 4, '16GT/s'),
(2, 2, 4, '16GT/s'),
(3, 3, 4, '16GT/s'),
(4, 4, 4, '16GT/s'),
(5, 5, 4, '16GT/s'),
(6, 6, 4, '16GT/s'),
(7, 7, 4, '16GT/s'),
(8, 8, 4, '16GT/s'),
(9, 9, 4, '16GT/s');

-- --------------------------------------------------------
--
-- Daten für Tabelle `tbl_grafikkarte`
--

INSERT INTO `tbl_grafikkarte` (`Grafikkarte_ID`, `Hersteller`, `benoetigte_Watt`, `Leistung`, `Grafikkartenspeicher`, `Strom_pins_anz`, `Abmessungen`, `Preis`, `PCI_e_ID`, `Netzteil_ID`) VALUES
(1, 'Nvidia', 400, 120, '6GB', '8', 255, 184.13, 1, 1),
(2, 'Nvidia', 650, 250, '8', '8', 328, 788, 2, 2),
(3, 'NVIDA MSI RTX 2080', 600, 300, '11', '8', 352, 1239, 2, 3),
(4, 'TITAN XP', 600, 350, '16', '8', 378, 1498, 2, 4),
(5, 'Radeon Rx 570', 300, 180, '8', '8', 420, 146.8, 2, 5),
(6, 'MSI RTX 2080 Super', 650, 250, '8', '8', 328, 788, 2, 6),
(7, 'Gigabyte RTX2070', 650, 350, '8', '8', 264.9, 419.9, 2, 7),
(8, 'Radeon RX5700', 750, 225, '8', '8', 236, 399, 2, 8),
(9, 'KF2A 2060', 500, 175, '8', '8', 340.8, 399, 2, 9),
(10, 'Gigabyte RTX2070', 600, 220, '8', '8', 286, 524.9, 2, 10);

-- --------------------------------------------------------

--
-- Daten für Tabelle `tbl_kunden`
--

INSERT INTO `tbl_kunden` (`Kunden_ID`, `Name`, `Vorname`, `telefonnummer`, `Ort`, `Strasse`, `Hausnummer`, `PLZ`, `EMail`) VALUES
(1, 'Maier', 'Peter', '0123545887546', 'Musterstadt', 'Musterstrasse', '20', '12345', 'petermaier@mustermail.de'),
(2, 'Schrimps', 'Kivi', '0215626156681', 'Mönchengladbach', 'Hauptstrasse', '40', '41061', 'kivischrimps@mustermail.d'),
(3, 'Bist', 'Guido', '015223401738', 'Viersen', 'Rahsterstraße', '50', '41747', 'Guidoderuverschämte@muste'),
(4, 'Giovanna', 'Giorno', '015564744874', 'Mönchengladbach', 'Requiemstraße', '10', '41061', 'Goldenexperience@No.de'),
(5, 'Fugo', 'Pannacotta', '015687421442', 'Mönchengladbach', 'Purplehazestrasse', '50', '41061', 'Runningaway@run.de'),
(6, 'clover', 'Albert', '0156454856484', 'Köln', 'Cloverstraße', '50', '50667', 'Madamada@blackclover.com'),
(7, 'Soos', 'Sylva', '01235648798', 'Köln', 'Cloverstraße', '60', '41061', 'nero@birdisdaword.com'),
(8, 'Oger', 'Shrek', '065544524652841', 'Mönchengladbach', 'allstarstraße', '20', '41061', 'shrekallstar@gmail.com'),
(9, 'the hedgehog', 'Sonic', '0000000000', 'Green Hill', 'Stagestrasse', '1', '111111', 'sonicthehedgehog@gmail.co');

-- --------------------------------------------------------

-- --------------------------------------------------------
-- 
--
-- Daten für Tabelle `tbl_ram_riegel_slot`
--

INSERT INTO `tbl_ram_riegel_slot` (`Ram_Slot_ID`, `Ram_Slot_typ`) VALUES
(1, 'DDR4'),
(2, 'DDR3'),
(3, 'DDR3L'),
(4, 'DDR2'),
(5, 'DDR'),
(6, 'SDR'),
(7, 'DIMM'),
(8, 'SO-DIMM'),
(9, 'mit-ECC'),
(10, 'NVDIMM');

--
-- Daten für Tabelle `tbl_ram`
--

INSERT INTO `tbl_ram` (`Ram_ID`, `Name`, `groesse`, `Taktung`, `Leistung`, `DDR_Typ_ID`, `Hersteller`, `Preis`) VALUES
(1, 'Crucial 4Gb', '4GB', '2400', NULL, 4, 'Crucial', 30.99),
(2, 'Crosair CMK16', '16 GB (2x8)', '3200', NULL, 4, 'Crosair', 78.9),
(3, 'Crosair Vengeance', '16 GB', '2400', NULL, 4, 'Crosair', 82.79),
(4, 'Crucial Ballistix', '16GB', '3200', NULL, 4, 'Crucial', 76.99),
(5, 'HyperX Fury', '16 GB (2x8)', '2400', NULL, 4, 'HyperX', 76.52),
(6, 'Ballistix Sport', '8GB x2', '3200', NULL, 4, 'Ballistix', 79.9),
(7, 'Ballistix Sport ', '32 GB 2x16', '3200', NULL, 4, 'Ballistix', 102.99),
(8, 'HyperX Fury', '16GB', '1860', NULL, 3, 'HyperX', 76.99),
(9, 'Crosair Vengeance ', '32', '4000', NULL, 4, 'Crosair', 137.79),
(10, 'Crosair', '64', '2400', NULL, 4, 'Crosair', 158.45),
(101, 'Aegis', '16GB', '2400', 1, 1, 'G.Skill', 58.9),
(102, 'HMT112S6TFR8C-G7', '1GB', '1066', 2, 2, 'SK-Hynix', 2.03),
(103, 'HMT31GR7BFR4A-H9', '8GB', '1333', 1, 3, 'SK-Hynix', 20),
(104, 'KVR533D2S4', '256', '533', 2, 4, 'Kingston', 0.01),
(105, 'Essentials', '1GB', '400', 3, 5, 'Mushkin', 7.2),
(106, 'TS32MSS64V6G', '256MB', '133', 3, 6, 'Transcend', 24.02),
(107, 'M386B4G70DM0', '32GB', '1866', 2, 7, 'Samsung', 75),
(108, 'CT16G4SFD8266', '16GB', '2666', 1, 8, 'Crucial', 54.76),
(109, 'OWC1333D3X9M048', '48GB', '1333', 2, 9, 'OWC', 174.9),
(110, 'Optane', '1TB', '2666', 1, 10, 'Intel', 2156.57);


