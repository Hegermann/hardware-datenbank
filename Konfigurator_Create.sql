-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 18. Jan 2020 um 21:11
-- Server-Version: 10.1.37-MariaDB
-- PHP-Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `pc`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_cpu`
--

CREATE TABLE `tbl_cpu` (
  `Cpu_Id` int(11) NOT NULL,
  `Sockel_ID` int(11) NOT NULL,
  `Name` varchar(15) DEFAULT NULL,
  `Multithread` tinyint(1) DEFAULT NULL,
  `Kerne` tinyint DEFAULT NULL, -- Uberarbeiten: Zahl?
  `Threats` varchar(3) DEFAULT NULL, -- Uberarbeiten: Zahl?
  `Taktrate` varchar(10) DEFAULT NULL,
  `Turbotakt` varchar(10) DEFAULT NULL,  -- Überarbeiten: Zahl?
  `Leistung` varchar(5) DEFAULT NULL, -- Überarbeiten: Zahl?
  `Hersteller` varchar(20) DEFAULT NULL,
  `Preis` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_grafikkarte`
--

CREATE TABLE `tbl_grafikkarte` (
  `Grafikkarte_ID` int(11) NOT NULL,
  `Hersteller` varchar(20) NOT NULL,
  `benoetigte_Watt` int(11) DEFAULT NULL,
  `Leistung` int(11) DEFAULT NULL,
  `Grafikkartenspeicher` varchar(30) NOT NULL, -- Überarbeiten: Zahl?
  `Strom_pins_anz` varchar(1) DEFAULT NULL,
  `Abmessungen` float DEFAULT NULL,
  `Preis` float DEFAULT NULL,
  `PCI_e_ID` int(11) DEFAULT NULL,
  `Netzteil_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_konfigurator`
--

CREATE TABLE `tbl_konfigurator` (
  `KonfigID` int(11) NOT NULL,
  `Kunden_ID` int(11) NOT NULL,
  `M_id` int(11) DEFAULT NULL,
  `Grafikkarte_ID` int(11) DEFAULT NULL,
  `Netzteil_ID` int(11) NOT NULL,
  `Cpu_ID` int(11) DEFAULT NULL,
  `Mass_ID` int(11) DEFAULT NULL, -- Wenn mehrere genommen werden?
  `Ram_ID` int(11) DEFAULT NULL, -- wenn mehrere genommen werden?
  `Datum` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_kunden`
--

CREATE TABLE `tbl_kunden` (
  `Kunden_ID` int(11) NOT NULL,
  `Name` varchar(25) DEFAULT NULL,
  `Vorname` varchar(25) DEFAULT NULL,
  `telefonnummer` varchar(30) DEFAULT NULL,
  `Ort` varchar(25) DEFAULT NULL,
  `Strasse` varchar(25) DEFAULT NULL,
  `Hausnummer` varchar(6) DEFAULT NULL,
  `PLZ` varchar(30) DEFAULT NULL, -- 30 erscheint etwas viel
  `EMail` varchar(85) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_mainboard`
--

CREATE TABLE `tbl_mainboard` (
  `M_id` int(11) NOT NULL,
  `M_Name` varchar(30) DEFAULT NULL,
  `M_Formfaktor` varchar(30) DEFAULT NULL,
  `Mainboard_Sockel_ID` int(11) DEFAULT NULL,
  `Netzteil_ID` int(11) NOT NULL,
  `Sockel_ID` int(11) NOT NULL,
  `PCI_e_slot_anz` varchar(2) DEFAULT NULL, -- Anzahl: Zahl?
  `PCI_e_ID` int(11) DEFAULT NULL,
  `Leistung` int(11) DEFAULT NULL,
  `Ram_Riegel_slot_anz` varchar(2) DEFAULT NULL, -- Anzahl: Zahl?
  `RAM_Slot_ID` int(11) DEFAULT NULL,
  `Hersteller` varchar(20) DEFAULT NULL,
  `Preis` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_massenspeicher`
--

CREATE TABLE `tbl_massenspeicher` (
  `Mass_ID` int(11) NOT NULL,
  `Speicherplatz` varchar(5) DEFAULT NULL, -- Zahl? Einheit?
  `Festplatten_Art` varchar(20) DEFAULT NULL,
  `Abmessungen` float DEFAULT NULL,
  `Uploadspeed` varchar(5) DEFAULT NULL, -- Zahl?
  `Downloadspeed` varchar(5) DEFAULT NULL, -- Zahl?
  `Hersteller` varchar(25) DEFAULT NULL,
  `Preis` float DEFAULT NULL,
  `Mass_SATA_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_netzteil`
--

CREATE TABLE `tbl_netzteil` (
  `Netzteil_ID` int(11) NOT NULL,
  `Hersteller` varchar(20) NOT NULL,
  `Effizienzklasse` varchar(20) NOT NULL,
  `Watt` int(11) NOT NULL,
  `Preis` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_netzteil_gpu`
--

CREATE TABLE `tbl_netzteil_gpu` (
  `Netzteil_ID` int(11) NOT NULL,
  `Grafikkarte_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_pci_e_slot`
--

CREATE TABLE `tbl_pci_e_slot` (
  `PCI_e_ID` int(11) NOT NULL,
  `PCI_e_slot` int(11) NOT NULL,
  `Version` float NOT NULL,
  `PCIe_Groesse` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_ram`
--

CREATE TABLE `tbl_ram` (
  `Ram_ID` int(11) NOT NULL,
  `Name` varchar(40) NOT NULL,
  `groesse` varchar(30) NOT NULL, -- Zahl, Einheit?
  `Taktung` varchar(10) NOT NULL,
  `Leistung` int(11) DEFAULT NULL,
  `DDR_Typ_ID` int(11) NOT NULL,
  `Hersteller` varchar(25) DEFAULT NULL,
  `Preis` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_ram_riegel_slot`
--

CREATE TABLE `tbl_ram_riegel_slot` (
  `Ram_Slot_ID` int(11) NOT NULL,
  `Ram_Slot_typ` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_sata_anschluss`
--

CREATE TABLE `tbl_sata_anschluss` (
  `SATA_Anschluss_ID` int(11) NOT NULL,
  `Mass_SATA_ID` int(11) DEFAULT NULL,
  `Sata_transferspeed` varchar(5) DEFAULT NULL -- Zahl?
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_sata_mainboard`
--

CREATE TABLE `tbl_sata_mainboard` (
  `SATA_ID` int(11) NOT NULL DEFAULT '0',
  `MainboardID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tbl_sockel`
--

CREATE TABLE `tbl_sockel` (
  `Sockel_ID` int(11) NOT NULL,
  `Sockel_Typ` varchar(20) DEFAULT NULL,
  `Pins` varchar(10) DEFAULT NULL -- Anzahl? Zahl?
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `tbl_cpu`
--
ALTER TABLE `tbl_cpu`
  ADD PRIMARY KEY (`Cpu_Id`),
  ADD KEY `FK_cpu_sockel` (`Sockel_ID`);

--
-- Indizes für die Tabelle `tbl_grafikkarte`
--
ALTER TABLE `tbl_grafikkarte`
  ADD PRIMARY KEY (`Grafikkarte_ID`),
  ADD KEY `FK_Grafik_PCI` (`PCI_e_ID`);

--
-- Indizes für die Tabelle `tbl_konfigurator`
--
ALTER TABLE `tbl_konfigurator`
  ADD PRIMARY KEY (`KonfigID`),
  ADD KEY `fk_Kunde` (`Kunden_ID`);

--
-- Indizes für die Tabelle `tbl_kunden`
--
ALTER TABLE `tbl_kunden`
  ADD PRIMARY KEY (`Kunden_ID`);

--
-- Indizes für die Tabelle `tbl_mainboard`
--
ALTER TABLE `tbl_mainboard`
  ADD PRIMARY KEY (`M_id`),
  ADD KEY `FK_Sockel_MB` (`Sockel_ID`),
  ADD KEY `FK_RAM_MB` (`RAM_Slot_ID`),
  ADD KEY `FK_PCIE_MB` (`PCI_e_ID`);

--
-- Indizes für die Tabelle `tbl_massenspeicher`
--
ALTER TABLE `tbl_massenspeicher`
  ADD PRIMARY KEY (`Mass_ID`),
  ADD KEY `FK_tbl_Massenspeicher_tbl_SATA_Anschluss` (`Mass_SATA_ID`);

--
-- Indizes für die Tabelle `tbl_netzteil`
--
ALTER TABLE `tbl_netzteil`
  ADD PRIMARY KEY (`Netzteil_ID`);

--
-- Indizes für die Tabelle `tbl_netzteil_gpu`
--
ALTER TABLE `tbl_netzteil_gpu`
  ADD PRIMARY KEY (`Netzteil_ID`,`Grafikkarte_ID`),
  ADD KEY `Grafikkarte_ID` (`Grafikkarte_ID`);

--
-- Indizes für die Tabelle `tbl_pci_e_slot`
--
ALTER TABLE `tbl_pci_e_slot`
  ADD PRIMARY KEY (`PCI_e_ID`);

--
-- Indizes für die Tabelle `tbl_ram`
--
ALTER TABLE `tbl_ram`
  ADD PRIMARY KEY (`Ram_ID`),
  ADD KEY `FK_RAM_SLOT` (`DDR_Typ_ID`);

--
-- Indizes für die Tabelle `tbl_ram_riegel_slot`
--
ALTER TABLE `tbl_ram_riegel_slot`
  ADD PRIMARY KEY (`Ram_Slot_ID`);

--
-- Indizes für die Tabelle `tbl_sata_anschluss`
--
ALTER TABLE `tbl_sata_anschluss`
  ADD PRIMARY KEY (`SATA_Anschluss_ID`);

--
-- Indizes für die Tabelle `tbl_sata_mainboard`
--
ALTER TABLE `tbl_sata_mainboard`
  ADD PRIMARY KEY (`MainboardID`,`SATA_ID`) USING BTREE,
  ADD KEY `SATA_ID` (`SATA_ID`);

--
-- Indizes für die Tabelle `tbl_sockel`
--
ALTER TABLE `tbl_sockel`
  ADD PRIMARY KEY (`Sockel_ID`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `tbl_konfigurator`
--
ALTER TABLE `tbl_konfigurator`
  MODIFY `KonfigID` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `tbl_cpu`
--
ALTER TABLE `tbl_cpu`
  ADD CONSTRAINT `FK_cpu_sockel` FOREIGN KEY (`Sockel_ID`) REFERENCES `tbl_sockel` (`Sockel_ID`);

--
-- Constraints der Tabelle `tbl_grafikkarte`
--
ALTER TABLE `tbl_grafikkarte`
  ADD CONSTRAINT `FK_Grafik_PCI` FOREIGN KEY (`PCI_e_ID`) REFERENCES `tbl_pci_e_slot` (`PCI_e_ID`);

--
-- Constraints der Tabelle `tbl_konfigurator`
--
ALTER TABLE `tbl_konfigurator`
  ADD CONSTRAINT `fk_Kunde` FOREIGN KEY (`Kunden_ID`) REFERENCES `tbl_kunden` (`Kunden_ID`);

--
-- Constraints der Tabelle `tbl_mainboard`
--
ALTER TABLE `tbl_mainboard`
  ADD CONSTRAINT `FK_PCIE_MB` FOREIGN KEY (`PCI_e_ID`) REFERENCES `tbl_pci_e_slot` (`PCI_e_ID`),
  ADD CONSTRAINT `FK_RAM_MB` FOREIGN KEY (`RAM_Slot_ID`) REFERENCES `tbl_ram_riegel_slot` (`Ram_Slot_ID`),
  ADD CONSTRAINT `FK_Sockel_MB` FOREIGN KEY (`Sockel_ID`) REFERENCES `tbl_sockel` (`Sockel_ID`),
  ADD CONSTRAINT `tbl_mainboard_ibfk_1` FOREIGN KEY (`M_id`) REFERENCES `tbl_sata_mainboard` (`MainboardID`);

--
-- Constraints der Tabelle `tbl_massenspeicher`
--
ALTER TABLE `tbl_massenspeicher`
  ADD CONSTRAINT `FK_tbl_Massenspeicher_tbl_SATA_Anschluss` FOREIGN KEY (`Mass_SATA_ID`) REFERENCES `tbl_sata_anschluss` (`SATA_Anschluss_ID`);

--
-- Constraints der Tabelle `tbl_netzteil`
--
ALTER TABLE `tbl_netzteil`
  ADD CONSTRAINT `tbl_netzteil_ibfk_1` FOREIGN KEY (`Netzteil_ID`) REFERENCES `tbl_netzteil_gpu` (`Netzteil_ID`);
-- Ändern, da in der Zwischentabelle das Attribut vorhanden ist!

--
-- Constraints der Tabelle `tbl_netzteil_gpu`
--
ALTER TABLE `tbl_netzteil_gpu`
  ADD CONSTRAINT `tbl_netzteil_gpu_ibfk_1` FOREIGN KEY (`Grafikkarte_ID`) REFERENCES `tbl_grafikkarte` (`Grafikkarte_ID`);
-- s.o.

--
-- Constraints der Tabelle `tbl_ram`
--
ALTER TABLE `tbl_ram`
  ADD CONSTRAINT `FK_RAM_SLOT` FOREIGN KEY (`DDR_Typ_ID`) REFERENCES `tbl_ram_riegel_slot` (`Ram_Slot_ID`);

--
-- Constraints der Tabelle `tbl_sata_mainboard`
--
ALTER TABLE `tbl_sata_mainboard`
  ADD CONSTRAINT `tbl_sata_mainboard_ibfk_1` FOREIGN KEY (`SATA_ID`) REFERENCES `tbl_sata_anschluss` (`SATA_Anschluss_ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
